const http = require("http");
const request = require("request");

http
  .get("http://jsonplaceholder.typicode.com/posts", (response) => {
    console.log(response);
    response.resume();
  })
  .on("error", (e) => {
    console.error(`Got error: ${e.message}`);
  });

request.get("http://jsonplaceholder.typicode.com/posts", (error, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log(response);
  }
});