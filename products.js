const yargs = require("yargs");
const fs = require("fs");

const args = yargs.command(
  "productId",
  "Product Id to delete",
  (yargs) => {
    yargs.positional("id", {
      describe: "Product id",
      type: "number",
      demandOption: true,
    });
  },
  (argv) => {
    console.log(`Inputed product id: ${argv._[1]}`);
  }
).argv;

const productsFileName = "products.json";
const productId = args._[1];

fs.readFile(productsFileName, "utf8", (err, data) => {
  if (err) {
    console.error(err);
  } else {
    const products = JSON.parse(data.toString());

    const filteredProducts = products.filter((p) => p.id !== productId);
    
    fs.writeFile(
      productsFileName,
      JSON.stringify(filteredProducts),
      (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log(filteredProducts);
        }
      }
    );
  }
});
