# Async

## Task

- Return data (GET) from url https://jsonplaceholder.typicode.com/posts using http, and after that using request module

- Use yargs to get productid from user input

- Read data from file Products.json and in callback write it to console output, remove Product with id you got from user input and save changes in Producst.json file.

- Handle errors which might occur in both cases by printing them on console
